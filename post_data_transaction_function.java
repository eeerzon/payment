private void PostDataTransaction(){
        Calendar calendar = Calendar.getInstance();
        Date today = Calendar.getInstance().getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 30);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String todayDate = dateFormat.format(today);
        Log.d("todayDate ", todayDate);

        Date nextDate = calendar.getTime();
        String expireDate = dateFormat.format(nextDate);
        Log.d("expireDate ", expireDate);

        //pay tipe 1 = Full Settlement, 2 = Installment, 3 = Mixed 1 & 2 Pay Type 2 & 3 only implement on BCA KlikPay channel
        String paytipe = "1";
        if (tempCodeChannel.equals("405")) {
            paytipe = "3";
        }

        FirebaseAuth auth = FirebaseAuth.getInstance();
        FirebaseUser user = auth.getCurrentUser();
        String email = user.getEmail();
        String cust_name = user.getDisplayName();

        String bill_no = String.valueOf(UUID.randomUUID());

        String body =
                "{" +
                "\"request\"" + ":" + "\"Post Data Transaction\"" + "," +
                "\"merchant_id\"" + ":" + "\"" + merchantId + "\"" + "," +
                "\"merchant\"" + ":" + "\"OMNIFIT\"" + "," +
                "\"bill_no\"" + ":" + "\"" + bill_no + "\"" + "," +
                "\"bill_reff\"" + ":" + "\"" + UUID.randomUUID() + "\"" + "," +
                "\"bill_date\"" + ":" + "\"" + todayDate + "\"" + "," +
                "\"bill_expired\"" + ":" + "\"" + expireDate + "\"" + "," +
                "\"bill_desc\"" + ":" + "\"Pembayaran #" + bill_no + "\"" + "," +
                "\"bill_currency\"" + ":" + "\"IDR\"" + "," +
                "\"bill_gross\"" + ":" + "\"0\"" + "," +
                "\"bill_miscfee\"" + ":" + "\"0\"" + "," +
                "\"bill_total\"" + ":" + "\"" + totalbayar + "\"" + "," +
                "\"cust_no\"" + ":" + "\"" + UUID.randomUUID() + "\"" + "," +
                "\"cust_name\"" + ":" + "\"" + cust_name + "\"" + "," +
                "\"payment_channel\"" + ":" + "\"" + tempCodeChannel + "\"" + "," +
                "\"pay_type\"" + ":" + "\"" + paytipe + "\"" + "," +
                "\"bank_userid\"" + ":" + "\"" + "\"" + "," +
                "\"msisdn\"" + ":" + "\"" + "62851" + "\"" + "," +
                "\"email\"" + ":" + "\"" + email + "\"" + "," +
                "\"terminal\"" + ":" + "\"" + "10" + "\"" + "," +
                "\"billing_name\"" + ":" + "\"" + "0" + "\"" + "," +
                "\"billing_lastname\"" + ":" + "\"" + "0" + "\"" + "," +
                "\"billing_address\"" + ":" + "\"" + "\"" + "," +
                "\"billing_address_city\"" + ":" + "\"" + "" + "\"" + "," +
                "\"billing_address_region\"" + ":" + "\"" + "" + "\"" + "," +
                "\"billing_address_state\"" + ":" + "\"" + "" + "\"" + "," +
                "\"billing_address_poscode\"" + ":" + "\"" + "" + "\"" + "," +
                "\"billing_msisdn\"" + ":" + "\"" + "" + "\"" + "," +
                "\"billing_address_country_code\"" + ":" + "\"" + "ID" + "\"" + "," +
                "\"receiver_name_for_shipping\"" + ":" + "\"" + "" + "\"" + "," +
                "\"shipping_lastname\"" + ":" + "\"" + "" + "\"" + "," +
                "\"shipping_address\"" + ":" + "\"" + "" + "\"" + "," +
                "\"shipping_address_city\"" + ":" + "\"" + "" + "\"" + "," +
                "\"shipping_address_region\"" + ":" + "\"" + "" + "\"" + "," +
                "\"shipping_address_state\"" + ":" + "\"" + "" + "\"" + "," +
                "\"shipping_address_poscode\"" + ":" + "\"" + "" + "\"" + "," +
                "\"shipping_msisdn\"" + ":" + "\"" + "" + "\"" + "," +
                "\"shipping_address_country_code\"" + ":" + "\"" + "ID" + "\"" + "," +
                "\"item\"" + ":" + jsonArray + "," +
                "\"reserve1\"" + ":" + "\"" + "\"" + "," +
                "\"reserve2\"" + ":" + "\"" + "\"" + "," +
                "\"signature\"" + ":" + "\"" + postTransactionSignature(bill_no) + "\"" +
                "}";

        Log.d("body ", body);

    }

    public String postTransactionSignature(String paramBill_no) {
        String userAndPass = userId + password + paramBill_no;
        char[] md5 = Hex.encodeHex(DigestUtils.md5(userAndPass));
        byte[] data = DigestUtils.sha1(new String(md5));
        char[] sha1 = Hex.encodeHex(data);
        return (new String(sha1));

    }